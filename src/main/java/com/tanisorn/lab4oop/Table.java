/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tanisorn.lab4oop;

/**
 *
 * @author uSeR
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1, player2, currentPlayer;
    private int row, col;
    private int count;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (count == 9) {
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }

    public boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer.getSymbol() && table[i][1] == currentPlayer.getSymbol() && table[i][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    public boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer.getSymbol() && table[1][i] == currentPlayer.getSymbol() && table[2][i] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    public boolean checkX1() {
         if (table[0][0] == currentPlayer.getSymbol() && table[1][1] ==currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()) {
            return true;
        }
        return false;
    }

    public boolean checkX2() {
        if (table[0][2] == currentPlayer.getSymbol() && table[1][1] ==currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()) {
            return true;
        }
        return false;
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player1.lose();
            player2.win();
        }
    }

    void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
            
        } else {
            currentPlayer = player1;
        }
    }
}
    
