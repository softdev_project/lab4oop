/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tanisorn.lab4oop;

import java.util.Scanner;

/**
 *
 * @author uSeR
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        newGame();
        printWelcome();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                isFinish = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                isFinish = true;
            }
            table.switchPlayer();
        }
        printResult();
        printContinue();
    }

    private void printWelcome() {
        System.out.println("Welcome to xo game!");
    }

    private void printTable() {
        char[][] t = table.getTable();
        System.out.println("_____________");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " | ");
            }
            System.out.println();
            System.out.println("_____________");
        }
    }

    private void printTurn() {
        System.out.print(table.getCurrentPlayer().getSymbol() + " turn. ");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row[1-3] and column[1-3]: ");
        int row = sc.nextInt() - 1;
        int col = sc.nextInt() - 1;
        if (!table.setRowCol(row, col)) {
            System.out.println(" Please Input Agian!!.");
            inputRowCol();
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println("Congratulations! Player " + table.getCurrentPlayer().getSymbol() + " Wins!!");
    }

    private void printDraw() {
        System.out.println("Draw!!");
    }

    private void printContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n) : ");
        String newGame = sc.next();
        if (newGame.equalsIgnoreCase("y")) {
            play();

        } else if (newGame.equalsIgnoreCase("n")) {
            System.out.println("------ Good Bye ------");
            System.exit(0);
        }
    }

    private void printResult() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
